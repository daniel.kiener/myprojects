# 0. Handlungsziele

Hello from Ajnur! Yes it works

Herzlich Willkommen auf den Doku-Seiten zum ÜK Modul 106 - Datenbanken abfragen, bearbeiten und warten. Viel Erfolg im Modul!

![](./img/kap0/hello.png)

# Handlungsziele und handlungsnotwendige Kenntnisse
    
??? success "1. Erarbeitet ein Datensicherheits- und Rollenkonzept und dokumentiert dieses."
    1. Kennt die Notwendigkeit zur Dokumentation eines Datensicherheits- und Rollenkonzepts. 
     2. Kennt den Begriff Rolle als Abstraktion einer bestimmten Benutzer- bzw. Zugriffsgruppe. 
     3. Kennt allgemeine Möglichkeiten eines Datenbankmanagementsystems (DBMS), um die Prinzipien der Datensicherheit und des Zugriffsschutzes umzusetzen.
     4. Kennt Inhalte eines Datensicherheitskonzeptes, damit die Zugriffsberechtigungen auf Teile der Datenbank klar geregelt werden können.

??? success "2. Setzt die Zugriffsberechtigungen (Rollen/Berechtigungen) gemäss Konzept zur Gewährleistung der Datensicherheit und des Datenschutzes."
    1. Kennt Befehle einer Datenkontrollsprache (DCL) um Benutzer und/oder Rollen zu verwalten. 
    2. Kennt Befehle einer Datenkontrollsprache (DCL), um Zugriffsberechtigungen an Benutzer und/oder Rollen zuzuweisen und zu verwalten. 

??? success "3. Führt Befehle zur Abfrage der Daten aus und nutzt Filter- sowie Aggregationsfunktionen."
    1. Kennt Befehle einer Datenbearbeitungssprache (DML) zur Abfrage von Datenbeständen und mit einfachen bis komplexen Filterfunktionen. 
    2. Kennt Befehle und Varianten einer Datenbearbeitungssprache (DML) zur Abfrage von Datenbeständen über mehrere Tabellen (JOIN). 
    3. Kennt Befehle einer Datenbearbeitungssprache (DML) zur gruppierten und bei Bedarf gruppenweise gefilterten Abfrage von Datenbeständen über eine und mehrere Tabellen. 
    4. Kennt Befehle einer Datenbearbeitungssprache (DML) zur Aggregation von Feldern sowie Textliteralen wie auch wichtige (Gruppen-)Hilfsfunktionen (Zufallszahlen, Anzahl, Maximum, Minimum, Durchschnitt, Summe).

??? success "4. Bearbeitet Daten und setzt Transaktionen ein falls nötig."
    1. Kennt die Notwendigkeit der Sicherung von verbundenen Aktionsschritten durch Transaktionen. 
    2. Kennt die Kriterien des ACID-Prinzips (Atomic, Constistent, Isolated, Durable), worüber Transaktionen charakterisiert werden. 
    3. Kennt Befehle einer Transaktionskontrollsprache (TCL) zur Definition von Transaktionsgrenzen und Sicherungspunkten in Manipulationsschritten von über mehreren Tabellen verteilten, einzufügenden oder zu ändernden Datensätzen.

??? success "5. Sichert Daten und Datenbankschema in einem Backup und stellt daraus die Daten sowie das Datenbankschema wieder her."
    1. Kennt Befehle eines Datenbankmanagementsystems (DBMS) zur Sicherung einer Datenbank (Daten und Datenbankschema) im ruhenden und laufenden Betrieb. 
    2. Kennt Befehle eines Datenbankmanagementsystems (DBMS) zur Wiederherstellung einer Datenbank (Daten und Datenbankschema) oder Teile davon.

??? success "6. Ändert oder migriert ein Datenbankschema und die Daten einer Datenbank"
    1. Kennt Befehle einer Datendefinitionssprache (DDL) zur Anpassung eines physischen Datenbankschemas und allenfalls notwendige Massnahmen zur Sicherstellung der Datenintegrität und -Vollständigkeit. 

??? success "7. Optimiert die Datenbank bezüglich Zugriffszeiten und Ressourcenbedarf"
    1. Kennt Befehle einer Datendefinitionssprache (DDL) zum Ergänzen von Indizes zur Beschleunigung von Abfragen auf bestimmten Feldern. 